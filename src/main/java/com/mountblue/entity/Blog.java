package com.mountblue.entity;

import javax.persistence.*;
import java.util.*;

@Entity(name = "blogs")
public class Blog extends Time {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int authorId;
    private String blogTitle;
    private String blogDescription;
    private String blogContent;
    private Date publishedAt;
    private boolean saveDraft;

    @Transient
    private String authorName;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "blogs_tags",
            joinColumns = @JoinColumn(name = "blog_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id"))
    private Set<Tag> tagSet;

    @OneToMany(mappedBy = "blog", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Comment> commentList;

    public int getId() {
        return id;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getBlogTitle() {
        return blogTitle;
    }

    public void setBlogTitle(String blogName) {
        this.blogTitle = blogName;
    }

    public String getBlogDescription() {
        return blogDescription;
    }

    public void setBlogDescription(String blogDescription) {
        this.blogDescription = blogDescription;
    }

    public String getBlogContent() {
        return blogContent;
    }

    public void setBlogContent(String blog) {
        this.blogContent = blog;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(Date publishAt) {
        this.publishedAt = publishAt;
    }

    public boolean getSaveDraft() {
        return saveDraft;
    }

    public void setSaveDraft(boolean saveDraft) {
        this.saveDraft = saveDraft;
    }

    public Set<Tag> getTagSet() {
        return tagSet;
    }

    public void setTagSet(Set<Tag> tag) {
        this.tagSet = tag;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

}
