package com.mountblue.entity;

import com.mountblue.annotation.ValidEmail;
import com.mountblue.annotation.ValidPassword;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@ValidPassword
public class FormInput {

    @NotBlank(message = "Field cannot be empty!")
    @Size(min = 2, max = 10, message = "User Name must be of min 2 and max 10 length!")
    String userName;
    @Size(min = 6, message = "Password length should be more than 6!")
    String password;
    @ValidEmail
    @NotBlank(message = "Field cannot be empty!")
    String email;

    @NotBlank(message = "Field cannot be empty!")
    String conformPassword;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConformPassword() {
        return conformPassword;
    }

    public void setConformPassword(String conformPassword) {
        this.conformPassword = conformPassword;
    }

}
