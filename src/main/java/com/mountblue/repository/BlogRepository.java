package com.mountblue.repository;

import com.mountblue.entity.Blog;
import com.mountblue.entity.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface BlogRepository extends JpaRepository<Blog, Integer> {

    List<Blog> findBySaveDraftAndAuthorId(Boolean value, Integer id);

    List<Blog> findByAuthorIdOrTagSetOrBlogTitleOrBlogDescriptionOrBlogContentOrderByPublishedAtDesc
            (Integer id, Tag tag, String title, String description, String content);

    List<Blog> findByAuthorIdOrBlogTitleOrBlogDescriptionOrBlogContentOrderByPublishedAtDesc
            (Integer id, String string, String string1, String string2);

    Page<Blog> findBySaveDraftFalseAndAuthorId(Integer id, Pageable pageable);

    Page<Blog> findBySaveDraftFalseAndAuthorIdAndTagSet(Integer id, Tag tag, Pageable pageable);

    Page<Blog> findAllBySaveDraftFalse(Pageable pageable);

    Set<Blog> findByAuthorId(Integer id);

}
