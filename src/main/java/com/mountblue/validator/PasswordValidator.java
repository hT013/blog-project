package com.mountblue.validator;

import com.mountblue.annotation.ValidPassword;
import com.mountblue.entity.FormInput;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<ValidPassword, Object> {
    @Override
    public void initialize(ValidPassword constraintAnnotation) {
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {
        FormInput formInput = (FormInput) object;
        return formInput.getPassword().equals(formInput.getConformPassword());
    }
}
