package com.mountblue.constant;

public class Constant {

    public static final String EMPTY = "";
    public static final String SPACE = " ";
    public static final String DOUBLE_QUOTE = "\"";

    public static final String EMAIL_FROM = "no-reply@ht.com";
    public static final String VERIFY_ACCOUNT_MESSAGE = "To conform this account please click here : ";
    public static final String VERIFY_ACCOUNT_LINK = "http://blog-website.ap-south-1.elasticbeanstalk.com/conform-account?token=";
    public static final String VERIFY_ACCOUNT_SUBJECT = "Verify Account";
    public static final String FORGOT_PASSWORD_MESSAGE = "To change password please click here : ";
    public static final String FORGOT_PASSWORD_LINK = "http://blog-website.ap-south-1.elasticbeanstalk.com/change-password?token=";
    public static final String FORGOT_PASSWORD_SUBJECT = "Change Password";

    public static final String EMAIL_REGEX = "[a-zA-Z0-9][a-zA-Z0-9]*[._]?[a-zA-Z0-9]+@[a-zA-Z]+[.][a-zA-Z]+";

    public static final String MESSAGE = "message";


}
