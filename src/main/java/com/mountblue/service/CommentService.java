package com.mountblue.service;

import com.mountblue.entity.Comment;
import com.mountblue.repository.CommentRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentService {

    private final CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public void save(Comment comment) {
        commentRepository.save(comment);
    }

    public Comment get(Integer id) {
        Optional<Comment> comment = commentRepository.findById(id);
        return comment.orElseGet(Comment::new);
    }

    public void delete(Integer id) {
        commentRepository.deleteById(id);
    }


}
