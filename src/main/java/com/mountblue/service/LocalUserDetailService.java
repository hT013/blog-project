package com.mountblue.service;

import com.mountblue.entity.User;
import com.mountblue.configuration.UserDetailsConfig;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class LocalUserDetailService implements UserDetailsService {

    private final UserService userService;

    public LocalUserDetailService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User user = userService.getUserDetails(name);
        if (user == null) {
            throw  new UsernameNotFoundException("User Name Not Found");
        }
        return new UserDetailsConfig(user);
    }

}
