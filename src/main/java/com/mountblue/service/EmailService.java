package com.mountblue.service;

import com.mountblue.entity.Token;
import com.mountblue.event.SendEmailEvent;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.UUID;

import static com.mountblue.constant.Constant.*;

@Service
@Component
public class EmailService {

    private final SendGrid sendGrid;
    private final TokenService tokenService;
    private final Logger logger;

    public EmailService(SendGrid sendGrid, TokenService tokenService) {
        this.sendGrid = sendGrid;
        this.tokenService = tokenService;
        this.logger = LoggerFactory.getLogger(EmailService.class);
    }

    @EventListener
    public void mail(SendEmailEvent sendEmailEvent) {
        Token tokenObj = new Token();
        tokenObj.setUser(sendEmailEvent.getUser());
        String token = UUID.randomUUID().toString();
        Email from = new Email(EMAIL_FROM);
        Email to = new Email(tokenObj.getUser().getEmail());
        Content content = new Content("text/plain", sendEmailEvent.getMessage() + token);
        Mail mail = new Mail(from, sendEmailEvent.getSubject(), to, content);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sendGrid.api(request);
            if (response.getStatusCode() == 202) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.HOUR, 24);
                tokenObj.setExpiryDate(calendar.getTime());
                tokenObj.setToken(token);
                tokenService.save(tokenObj);
            } else {
                logger.warn("Not able to send mail to user response : {}", response.getStatusCode());
            }
        } catch (Exception e) {
            logger.error("Error in mail listener ", e);
        }
    }

}
