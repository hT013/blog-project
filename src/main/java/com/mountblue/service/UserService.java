package com.mountblue.service;

import com.mountblue.entity.User;
import com.mountblue.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public List<User> listAll() {
        return userRepository.findAll();
    }

    public User get(Integer id) {
        Optional<User> user = userRepository.findById(id);
        return user.orElseGet(User::new);
    }

    public Integer getId(String name) {
        User user = userRepository.findByUserName(name);
        if (user != null) {
            return user.getId();
        }
        return 0;
    }

    public User getUserDetails(String name) {
        return userRepository.findByUserName(name);
    }

    public Boolean userExist(String name) {
        return userRepository.findByUserName(name) != null;
    }

    public boolean emailExist(String email) {
        return userRepository.findByEmail(email) != null;
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void delete(User user) {
        userRepository.delete(user);
    }

}
