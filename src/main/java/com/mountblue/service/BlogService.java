package com.mountblue.service;

import com.mountblue.entity.Tag;
import com.mountblue.repository.BlogRepository;
import com.mountblue.entity.Blog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.mountblue.constant.Constant.*;


@Service
public class BlogService {

    private final BlogRepository blogRepository;

    public BlogService(BlogRepository blogRepository) {
        this.blogRepository = blogRepository;
    }

    public void save(Blog blog) {
        blogRepository.save(blog);
    }

    public Blog get(Integer id) {
        Optional<Blog> blog = blogRepository.findById(id);
        return blog.orElseGet(Blog::new);
    }

    public Integer pageNum(Integer page) {
        page--;
        if (page < 0) {
            page = 0;
        }
        return page;
    }

    public Page<Blog> getPage(Integer page, Integer pageSize, String sortBy) {
        page = pageNum(page);
        PageRequest pageRequest = PageRequest.of(page, pageSize, Sort.by(sortBy).descending());
        return blogRepository.findAllBySaveDraftFalse(pageRequest);
    }

    public void delete(Integer id) {
        blogRepository.deleteById(id);
    }

    public Blog update(Integer id, Blog blog) {
        Blog blogObject = get(id);
        blogObject.setBlogContent(blog.getBlogContent());
        blogObject.setBlogDescription(blog.getBlogDescription());
        blogObject.setBlogTitle(blog.getBlogTitle());
        blogObject.setUpdatedAt(blog.getUpdatedAt());
        blogObject.setSaveDraft(blog.getSaveDraft());
        if (blogObject.getPublishedAt() == null) {
            if (blog.getPublishedAt() == null && !blog.getSaveDraft()) {
                blogObject.setPublishedAt(new Date());
            } else {
                blogObject.setPublishedAt(blog.getPublishedAt());
            }
        }
        return blogObject;
    }

    public List<Blog> getSavedBlogId(Integer id) {
        return blogRepository.findBySaveDraftAndAuthorId(true, id);
    }

    public List<Blog> getPublishedBlog(Integer id) {
        return blogRepository.findBySaveDraftAndAuthorId(false, id);
    }

    public Page<Blog> getSortedByField(String field, String order, Integer page, Integer limit) {
        String[] string = field.split(DOUBLE_QUOTE);
        for (String str : string) {
            if (!str.isEmpty()) {
                field = str;
                break;
            }
        }
        page = pageNum(page);
        if (order != null && (order.equals("\"desc\"") || order.equals("desc"))) {
            return blogRepository.findAllBySaveDraftFalse(PageRequest.of(page, limit, Sort.by(field).descending()));
        }
        return blogRepository.findAllBySaveDraftFalse(PageRequest.of(page, limit, Sort.by(field).ascending()));
    }

    public Page<Blog> searchAuthorId(Integer id, String field, Integer page, Integer limit) {
        page = pageNum(page);
        return blogRepository.findBySaveDraftFalseAndAuthorId(id,
                PageRequest.of(page, limit, Sort.by(field).descending()));
    }

    public Page<Blog> searchAuthorIdAndTag(Integer id, String field, Integer page, Integer limit, Tag tag) {
        page = pageNum(page);
        if (tag.getId() == 0) {
            tag = null;
        }
        return blogRepository.findBySaveDraftFalseAndAuthorIdAndTagSet(id, tag,
                PageRequest.of(page, limit, Sort.by(field).descending()));
    }

    public List<Blog> searchInBlog(String string, Integer id, Tag tag) {
        List<Blog> blogs;
        if (tag.getName() == null) {
            blogs = blogRepository.findByAuthorIdOrBlogTitleOrBlogDescriptionOrBlogContentOrderByPublishedAtDesc
                    (id, string, string, string);
        } else {
            blogs = blogRepository.findByAuthorIdOrTagSetOrBlogTitleOrBlogDescriptionOrBlogContentOrderByPublishedAtDesc
                    (id, tag, string, string, string);
        }
        List<Blog> blogList = new ArrayList<>();
        blogs.forEach(blog -> {
            if (!blog.getSaveDraft()) {
                blogList.add(blog);
            }
        });
        return blogList;
    }

    public List<Blog> getPageData(List<Blog> blogs, Integer index, Integer limit) {
        List<Blog> blogList = new ArrayList<>();
        while (index < blogs.size() && limit != 0) {
            blogList.add(blogs.get(index));
            index++;
            limit--;
        }
        return blogList;
    }

    public Set<Blog> findByAuthorId(Integer id) {
        return blogRepository.findByAuthorId(id);
    }

}
