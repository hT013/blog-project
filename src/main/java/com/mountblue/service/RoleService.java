package com.mountblue.service;

import com.mountblue.entity.Role;
import com.mountblue.repository.RoleRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public void save(Role role) {
        roleRepository.save(role);
    }

    public Role getByName(String name) {
        return roleRepository.findByName(name);
    }

    public Role get(int id) {
        return roleRepository.findById(id).orElseGet(Role::new);
    }

}
