package com.mountblue.service;

import com.mountblue.entity.Blog;
import com.mountblue.entity.Tag;
import com.mountblue.repository.TagRepository;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.mountblue.constant.Constant.*;

@Service
public class TagService {

    private final TagRepository tagRepository;

    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public Set<Tag> save(String tags, Blog blog) {
        Set<String> tagList = new HashSet<>(Arrays.asList(tags.split(SPACE)));
        Set<Tag> tagSet = new HashSet<>();
        HashMap<String, Integer> hashMap = new HashMap<>();
        listAll().forEach(tag -> hashMap.put(tag.getName(), tag.getId()));
        tagList.forEach(tag -> {
            if (!tag.isEmpty()) {
                Tag tagObject;
                if (hashMap.containsKey(tag)) {
                    tagObject = get(hashMap.get(tag));
                } else {
                    tagObject = new Tag();
                    tagObject.setName(tag);
                }
                Set<Blog> blogSet = tagObject.getBlogs();
                if (blogSet == null) {
                    blogSet = new HashSet<>();
                }
                blogSet.add(blog);
                tagObject.setBlogs(blogSet);
                tagSet.add(tagObject);
                tagRepository.save(tagObject);
            }
        });
        return tagSet;
    }

    public List<Tag> listAll() {
        return tagRepository.findAll();
    }

    public Tag get(Integer id) {
        Optional<Tag> tag = tagRepository.findById(id);
        return tag.orElseGet(Tag::new);
    }

    public Tag getByName(String tag) {
        Optional<Tag> tagOb = tagRepository.findByName(tag);
        return tagOb.orElseGet(Tag::new);
    }

    public void delete(Set<Tag> tagSet) {
        tagSet.forEach(tag -> tagRepository.deleteById(tag.getId()));
    }


}
