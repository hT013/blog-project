package com.mountblue;

import com.mountblue.entity.Role;
import com.mountblue.entity.User;
import com.mountblue.service.RoleService;
import com.mountblue.service.UserService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class BlogApplication {

    private final UserService userService;
    private final RoleService roleService;

    @PostConstruct
    public void initialize() {
        if (roleService.get(1).getId() == 0) {
            Role role = new Role();
            role.setName("ROLE_ADMIN");
            Role role1 = new Role();
            role1.setName("ROLE_USER");
            roleService.save(role);
            roleService.save(role1);
        }
        if (!userService.userExist("admin")) {
            User user = new User();
            user.setUserName("admin");
            user.setEmail("hitesh@ht.com");
            user.setIsEnabled(true);
            user.setRole(roleService.getByName("ROLE_ADMIN"));
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            user.setPassword(bCryptPasswordEncoder.encode("admin@123"));
            userService.save(user);
        }
    }

    public BlogApplication(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    public static void main(String[] args) {
        SpringApplication.run(BlogApplication.class, args);
    }


}
