package com.mountblue.controller;

import com.mountblue.entity.Comment;
import com.mountblue.entity.User;
import com.mountblue.service.BlogService;
import com.mountblue.service.CommentService;
import com.mountblue.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CommentController {

    private final CommentService commentService;
    private final UserService userService;
    private final BlogService blogService;
    private final Logger logger;

    public CommentController(CommentService commentService, UserService userService, BlogService blogService) {
        this.commentService = commentService;
        this.userService = userService;
        this.blogService = blogService;
        logger = LoggerFactory.getLogger(CommentController.class);
    }

    @RequestMapping(value = "/comment/add-comment/{id}")
    public String addComment(@PathVariable int id, @RequestParam(name = "comment") String comment,
                             Authentication authentication) {
        User user = userService.getUserDetails(authentication.getName());
        Comment commentObj = new Comment();
        commentObj.setComment(comment);
        commentObj.setUserName(user.getUserName());
        commentObj.setEmail(user.getEmail());
        commentObj.setBlog(blogService.get(id));
        commentService.save(commentObj);
        logger.info("Add comment in blog : {}", id);
        return "redirect:/show-blog?id=" + id;
    }

    @RequestMapping(value = "/comment/delete-comment/{id}/{commentId}")
    public String delComment(@PathVariable int id, @PathVariable int commentId) {
        commentService.delete(commentId);
        logger.info("delete comment in blog : {}", id);
        return "redirect:/show-blog?id=" + id;
    }

}
