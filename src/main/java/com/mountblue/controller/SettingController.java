package com.mountblue.controller;

import com.mountblue.entity.Blog;
import com.mountblue.entity.FormInput;
import com.mountblue.entity.User;
import com.mountblue.event.SendEmailEvent;
import com.mountblue.service.BlogService;
import com.mountblue.service.RoleService;
import com.mountblue.service.TokenService;
import com.mountblue.service.UserService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import static com.mountblue.constant.Constant.*;

@Controller
public class SettingController {

    private final BlogService blogService;
    private final UserService userService;
    private final RoleService roleService;
    private final TokenService tokenService;
    private final ApplicationEventPublisher applicationEventPublisher;

    public SettingController(UserService userService, ApplicationEventPublisher applicationEventPublisher, RoleService roleService, BlogService blogService, TokenService tokenService) {
        this.userService = userService;
        this.applicationEventPublisher = applicationEventPublisher;
        this.roleService = roleService;
        this.blogService = blogService;
        this.tokenService = tokenService;
    }

    private void addUsers(Model model, Authentication authentication) {
        if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
            List<User> users = new ArrayList<>();
            userService.listAll().forEach(user -> {
                if (!authentication.getName().equals(user.getUserName())) {
                    users.add(user);
                }
            });
            model.addAttribute("users", users);
            model.addAttribute("user", new User());
        }
    }

    @RequestMapping("/settings")
    public String setting(Model model, Authentication authentication) {
        addUsers(model, authentication);
        User user = userService.getUserDetails(authentication.getName());
        FormInput formInput = new FormInput();
        formInput.setEmail(user.getEmail());
        formInput.setUserName(user.getUserName());
        model.addAttribute("form", formInput);
        return "setting";
    }

    @RequestMapping("/settings/update-email")
    public String changeEmail(@ModelAttribute FormInput formInput, Model model, Authentication authentication) {
        if (userService.emailExist(formInput.getEmail().toLowerCase()) || !Pattern.matches(EMAIL_REGEX, formInput.getEmail())) {
            model.addAttribute("emailError", "Email already Exists");
            if (!Pattern.matches(EMAIL_REGEX, formInput.getEmail())) {
                model.addAttribute("emailError", "Invalid Email");
            }
            addUsers(model, authentication);
            formInput.setUserName(authentication.getName());
            model.addAttribute("form", formInput);
            return "setting";
        }
        User user = userService.getUserDetails(authentication.getName());
        user.setEmail(formInput.getEmail());
        user.setIsEnabled(false);
        userService.save(user);
        SendEmailEvent sendEmailEvent = new SendEmailEvent(this, user,
                VERIFY_ACCOUNT_MESSAGE + VERIFY_ACCOUNT_LINK, VERIFY_ACCOUNT_SUBJECT);
        applicationEventPublisher.publishEvent(sendEmailEvent);
        return "redirect:/logout";
    }

    @RequestMapping("/settings/update-password")
    public String updatePassword(@ModelAttribute FormInput formInput, Model model, Authentication authentication) {
        if (formInput.getPassword().length() < 6 || !formInput.getPassword().equals(formInput.getConformPassword())) {
            addUsers(model, authentication);
            formInput.setUserName(authentication.getName());
            formInput.setEmail(userService.getUserDetails(formInput.getUserName()).getEmail());
            model.addAttribute("form", formInput);
            model.addAttribute("passwordError", "password length should be greater than or equal to 6");
            if (!formInput.getPassword().equals(formInput.getConformPassword())) {
                model.addAttribute("passwordError", "Password should match");
            }
            return "setting";
        }
        User user = userService.getUserDetails(authentication.getName());
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        user.setPassword(bCryptPasswordEncoder.encode(formInput.getPassword()));
        userService.save(user);
        model.addAttribute("message", "Password updated successfully");
        return "message";
    }

    @RequestMapping("/settings/admin/make-admin/{user}")
    public String makeAdmin(@PathVariable String user) {
        User userObj = userService.getUserDetails(user);
        userObj.setRole(roleService.getByName("ROLE_ADMIN"));
        userService.save(userObj);
        return "redirect:/settings";
    }

    @RequestMapping("/settings/admin/remove-admin/{user}")
    public String removeAdmin(@PathVariable String user) {
        User userObj = userService.getUserDetails(user);
        userObj.setRole(roleService.getByName("ROLE_USER"));
        userService.save(userObj);
        return "redirect:/settings";
    }

    @RequestMapping("/settings/admin/delete-user/{user}")
    public String deleteUser(@PathVariable String user) {
        User userObj = userService.getUserDetails(user);
        tokenService.findTokenByUser(userObj.getId()).forEach(tokenService::delete);
        Set<Blog> blogSet = blogService.findByAuthorId(userObj.getId());
        blogSet.forEach(blog -> blogService.delete(blog.getId()));
        userService.delete(userObj);
        return "redirect:/settings";
    }

    @RequestMapping("/settings/admin/update-email")
    public String updateEmail(@ModelAttribute User user, Model model) {
        User userDetails = userService.get(user.getId());
        userDetails.setEmail(user.getEmail());
        if (userService.emailExist(user.getEmail().toLowerCase()) || !Pattern.matches(EMAIL_REGEX, user.getEmail())) {
            model.addAttribute("emailError", "Email already Exists");
            if (!Pattern.matches(EMAIL_REGEX, user.getEmail())) {

                model.addAttribute("emailError", "Invalid Email");
            }
            model.addAttribute("user", userDetails);
            return "admin";
        }
        userService.save(userDetails);
        return "redirect:/settings";
    }

    @RequestMapping("/settings/admin/user")
    public String userSettings(@RequestParam Integer id, Model model) {
        model.addAttribute("user", userService.get(id));
        return "admin";
    }

}
