package com.mountblue.controller;

import com.mountblue.entity.Blog;
import com.mountblue.entity.Tag;
import com.mountblue.service.BlogService;
import com.mountblue.service.TagService;
import com.mountblue.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

@Controller
public class HomeController {

    private Integer page;
    private Integer limit;

    private final BlogService blogService;
    private final UserService userService;
    private final TagService tagService;
    private final Logger logger;

    public HomeController(BlogService blogService, UserService userService, TagService tagService) {
        this.blogService = blogService;
        this.userService = userService;
        this.tagService = tagService;
        this.logger = LoggerFactory.getLogger(HomeController.class);
    }

    private void setValues(Model model, int pageNum, int limit) {
        model.addAttribute("pageNumber", pageNum);
        model.addAttribute("limit", limit);
    }

    private void setPageAndLimit(Map<String, String> map) {
        page = 1;
        limit = 10;
        try {
            if (map.get("page") != null) {
                page = Integer.parseInt(map.get("page"));
            }
            if (map.get("limit") != null) {
                limit = Integer.parseInt(map.get("limit"));
            }
        } catch (Exception e) {
            logger.error("Illegal Number send in request",e);
        }
    }

    @RequestMapping(value = "/")
    public String index(Model model, @RequestParam Map<String, String> map) {
        setPageAndLimit(map);
        List<Blog> list = new ArrayList<>();
        if (map.get("sortField") != null || map.get("search") != null || map.get("authorId") != null) {
            if (map.get("sortField") != null) {
                list = sortField(model, map);
            }
            if (map.get("search") != null) {
                list = search(model, map);
            }
            if (map.get("authorId") != null) {
                list = authorIDTag(model, map);
            }
        } else {
            list = pagination(model);
            model.addAttribute("home", true);
        }
        Set<Tag> tags = new HashSet<>();
        list.forEach(blog -> {
            blog.setAuthorName(userService.get(blog.getAuthorId()).getUserName());
            if (tags.size() <= 10) {
                tags.addAll(blog.getTagSet());
            }
        });
        model.addAttribute("tags", tags);
        model.addAttribute("list", list);
        return "index";
    }

    private List<Blog> search(Model model, Map<String, String> map) {
        String field = convertParam("search", map);
        Integer id = userService.getId(field);
        Tag tag = tagService.getByName(field);
        List<Blog> blogList = blogService.searchInBlog(field, id, tag);
        setValues(model, (int) Math.ceil(blogList.size() / (double) limit), limit);
        return blogService.getPageData(blogList, (page - 1) * limit, limit);

    }

    private List<Blog> sortField(Model model, Map<String, String> map) {
        String field = map.get("sortField");
        Page<Blog> blogPage;
        blogPage = blogService.getSortedByField(field, map.get("order"), page, limit);
        setValues(model, blogPage.getTotalPages(), limit);
        return blogPage.toList();
    }

    private List<Blog> pagination(Model model) {
        Page<Blog> blogPage = blogService.getPage(page, limit, "publishedAt");
        setValues(model, blogPage.getTotalPages(), limit);
        return blogPage.toList();
    }

    private List<Blog> authorIDTag(Model model, Map<String, String> map) {
        Page<Blog> blogPage;
        if (map.get("tagId") != null && !map.get("tagId").isEmpty()) {
            blogPage = blogService.searchAuthorIdAndTag(Integer.parseInt(convertParam("authorId", map)),
                    "publishedAt", page, limit, tagService.get(Integer.parseInt(convertParam("tagId", map))));
        } else {
            blogPage = blogService.searchAuthorId(Integer.parseInt(convertParam("authorId", map)),
                    "publishedAt", page, limit);
        }
        setValues(model, blogPage.getTotalPages(), limit);
        return blogPage.toList();
    }

    private String convertParam(String string, Map<String, String> map) {
        String[] strings = map.get(string).split("\"");
        String field = "";
        for (String str : strings) {
            if (!str.isEmpty()) {
                field = str;
                break;
            }
        }
        return field;
    }

}
