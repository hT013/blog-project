package com.mountblue.controller;

import com.mountblue.entity.FormInput;
import com.mountblue.entity.Token;
import com.mountblue.entity.User;
import com.mountblue.event.SendEmailEvent;
import com.mountblue.service.RoleService;
import com.mountblue.service.TokenService;
import com.mountblue.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import static com.mountblue.constant.Constant.*;

import javax.validation.Valid;
import java.util.Date;

@Controller
public class UserController {

    private final ApplicationEventPublisher applicationEventPublisher;
    private final UserService service;
    private final TokenService tokenService;
    private final RoleService roleService;
    private final Logger logger;

    public UserController(UserService service, TokenService tokenService,
                          ApplicationEventPublisher applicationEventPublisher, RoleService roleService) {
        this.service = service;
        this.tokenService = tokenService;
        this.applicationEventPublisher = applicationEventPublisher;
        this.roleService = roleService;
        this.logger = LoggerFactory.getLogger(UserController.class);
    }

    private boolean tokenCheck(Token token, Model model) {
        boolean expired = false;
        if (new Date().getTime() - token.getExpiryDate().getTime() > 0) {
            expired = true;
            model.addAttribute(MESSAGE, "Token expired");
        }
        return !expired;
    }

    @RequestMapping("/sign-up")
    public String register(Model model, Authentication authentication) {
        model.addAttribute("formInput", new FormInput());
        if (authentication != null) {
            return "redirect:/";
        }
        return "sign-up";
    }

    @PostMapping("/add-user")
    public String addUser(@Valid @ModelAttribute FormInput formInput, BindingResult bindingResult, Model model) {
        if (service.userExist(formInput.getUserName())) {
            bindingResult.rejectValue("userName", "error.userName", "User already exist!");
        }
        if (service.emailExist(formInput.getEmail().toLowerCase())) {
            bindingResult.rejectValue("email", "error.email", "E-mail already exist!");
        }
        if (bindingResult.hasErrors()) {
            return "sign-up";
        }
        User user1 = new User();
        user1.setEmail(formInput.getEmail().toLowerCase());
        user1.setUserName(formInput.getUserName());
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        user1.setPassword(bCryptPasswordEncoder.encode(formInput.getPassword()));
        user1.setIsEnabled(false);
        user1.setRole(roleService.getByName("ROLE_USER"));
        service.save(user1);
        SendEmailEvent sendEmailEvent = new SendEmailEvent(this, user1,
                VERIFY_ACCOUNT_MESSAGE + VERIFY_ACCOUNT_LINK, VERIFY_ACCOUNT_SUBJECT);
        applicationEventPublisher.publishEvent(sendEmailEvent);
        model.addAttribute(MESSAGE, "Conformation link send to your e-mail account");
        logger.info("User added successfully")
        ;
        return MESSAGE;
    }

    @RequestMapping("/sign-in")
    public String signIn(Authentication authentication) {
        if (authentication != null) {
            return "redirect:/";
        }
        return "sign-in";
    }

    @RequestMapping("/conform-account")
    public String conformAccount(@RequestParam String token, Model model) {
        Token tokenObj = tokenService.findToken(token);
        if (tokenObj != null) {
            if (tokenCheck(tokenObj, model)) {
                User user = service.getUserDetails(tokenObj.getUser().getUserName());
                user.setIsEnabled(true);
                service.save(user);
                model.addAttribute(MESSAGE, "Account verified");
            }
        } else {
            model.addAttribute(MESSAGE, "Link is invalid or broken");
        }
        return MESSAGE;
    }

    @RequestMapping("/forgot-password")
    public String forgotPassword(Model model) {
        model.addAttribute("error", EMPTY);
        model.addAttribute("form", new FormInput());
        return "enter-email";
    }

    @RequestMapping("/resend-link")
    public String resendLink(Model model) {
        model.addAttribute("error", EMPTY);
        model.addAttribute("form", new FormInput());
        return "enter-email";
    }

    @RequestMapping("/send-link")
    public String sendLink(Model model, @ModelAttribute FormInput form) {
        if (!form.getEmail().equals(EMPTY)) {
            User user = service.getUserByEmail(form.getEmail());
            if (user != null) {
                SendEmailEvent sendEmailEvent;
                if (user.getIsEnabled()) {
                    sendEmailEvent = new SendEmailEvent(this, user,
                            FORGOT_PASSWORD_MESSAGE + FORGOT_PASSWORD_LINK, FORGOT_PASSWORD_SUBJECT);
                } else {
                    sendEmailEvent = new SendEmailEvent(this, user,
                            VERIFY_ACCOUNT_MESSAGE + VERIFY_ACCOUNT_LINK, VERIFY_ACCOUNT_SUBJECT);
                }
                applicationEventPublisher.publishEvent(sendEmailEvent);
                model.addAttribute(MESSAGE, "Link send to your e-mail account");
                return MESSAGE;
            }
            model.addAttribute("error", "Email doesn't exist");
        } else {
            model.addAttribute("error", "Enter email");
        }
        model.addAttribute("form", new FormInput());
        return "enter-email";
    }

    @RequestMapping("/change-password")
    public String changePassword(@RequestParam String token, Model model) {
        Token tokenObj = tokenService.findToken(token);
        if (tokenObj != null) {
            model.addAttribute("error", EMPTY);
            model.addAttribute("token", token);
            model.addAttribute("formInput", new FormInput());
            return "change-password";
        }
        model.addAttribute(MESSAGE, "Link is invalid or broken");
        return MESSAGE;
    }

    @RequestMapping("/save-password")
    public String savePassword(@RequestParam String token, @ModelAttribute FormInput formInput, Model model) {
        if (formInput.getPassword().length() >= 6 && formInput.getPassword().equals(formInput.getConformPassword())) {
            Token tokenObj = tokenService.findToken(token);
            if (tokenObj != null) {
                if (tokenCheck(tokenObj, model)) {
                    User user = service.getUserDetails(tokenObj.getUser().getUserName());
                    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
                    user.setPassword(bCryptPasswordEncoder.encode(formInput.getPassword()));
                    service.save(user);
                    model.addAttribute(MESSAGE, "Password Changed Successfully");
                }
            } else {
                model.addAttribute(MESSAGE, "Invalid token");
            }
            return MESSAGE;
        }
        model.addAttribute("error", "Password should match");
        if (formInput.getPassword().length() < 6) {
            model.addAttribute("error", "password length should be greater or equal to 6");
        }
        model.addAttribute("token", token);
        model.addAttribute("formInput", new FormInput());
        return "change-password";
    }

}
