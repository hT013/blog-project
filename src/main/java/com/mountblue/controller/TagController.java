package com.mountblue.controller;

import com.mountblue.entity.Blog;
import com.mountblue.entity.Tag;
import com.mountblue.service.TagService;
import com.mountblue.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashSet;
import java.util.Set;

@Controller
public class TagController {

    private final TagService tagService;
    private final UserService userService;

    public TagController(TagService tagService, UserService userService) {
        this.tagService = tagService;
        this.userService = userService;
    }

    @RequestMapping(value = "/filter/tag/{tag}")
    public String filterByTag(@PathVariable String tag, Model model) {
        Set<Blog> blogSet = new HashSet<>();
        Set<Tag> tags = new HashSet<>();
        Tag tagObject = tagService.getByName(tag);
        tags.add(tagObject);
        tagObject.getBlogs().forEach(blog -> {
            blog.setAuthorName(userService.get(blog.getAuthorId()).getUserName());
            if (!blog.getSaveDraft()) {
                blogSet.add(blog);
            }
        });
        model.addAttribute("tags", tags);
        model.addAttribute("list", blogSet);
        model.addAttribute("pageNumber", 1);
        model.addAttribute("limit", blogSet.size());
        return "index";
    }

}
