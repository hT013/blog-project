package com.mountblue.controller;

import com.mountblue.entity.Blog;
import com.mountblue.entity.Comment;
import com.mountblue.entity.Tag;
import com.mountblue.entity.User;
import com.mountblue.service.BlogService;
import com.mountblue.service.TagService;
import com.mountblue.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static com.mountblue.constant.Constant.*;

@Controller
public class BlogController {

    private final BlogService blogService;
    private final UserService userService;
    private final TagService tagService;
    private final Logger logger;

    public BlogController(BlogService blogService, UserService userService, TagService tagService) {
        this.blogService = blogService;
        this.userService = userService;
        this.tagService = tagService;
        logger = LoggerFactory.getLogger(BlogController.class);
    }

    private void setBlogAndTag(Model model, Blog blog, Tag tag) {
        model.addAttribute("blog", blog);
        model.addAttribute("tag", tag);
    }

    private void setListAndUser(Model model, List<Blog> blogs, User user) {
        model.addAttribute("list", blogs);
        model.addAttribute("user", user);
    }

    private boolean checkAccess(int id, Authentication authentication) {
        return (userService.getUserDetails(authentication.getName()).getId() == blogService.get(id).getAuthorId() ||
                authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN")));
    }

    @RequestMapping(value = "/blog/new-blog")
    public String newPost(Model model) {
        setBlogAndTag(model, new Blog(), new Tag());
        return "new-post";
    }

    @RequestMapping(value = "/blog/update-blog")
    public String updatePost(@RequestParam Integer id, Model model, Authentication authentication) {
        if (checkAccess(id, authentication)) {
            Blog blog = blogService.get(id);
            Tag tag = new Tag();
            tag.setName(EMPTY);
            logger.info("update blog id : {}", id);
            setBlogAndTag(model, blog, tag);
            return "new-post";
        }
        return "redirect:/show-blog?id=" + id;
    }

    @RequestMapping(value = "/blog/delete-blog")
    public String deletePost(@RequestParam Integer id, Authentication authentication) {
        if (checkAccess(id, authentication)) {
            tagService.delete(blogService.get(id).getTagSet());
            blogService.delete(id);
            logger.info("deleted blog : {}", id);
            return "redirect:/";
        }
        return "redirect:/show-blog?id=" + id;
    }

    @RequestMapping(value = "/author")
    public String author(@RequestParam(name = "id", required = false) Integer id, Model model,
                         Authentication authentication) {
        if (authentication != null && id == null) {
            id = userService.getUserDetails(authentication.getName()).getId();
        }
        setListAndUser(model, blogService.getPublishedBlog(id), userService.get(id));
        return "author";
    }

    @RequestMapping(value = "/blog/author-saved-blog")
    public String authorSavedBlog(@RequestParam(name = "id") int id, Model model, Authentication authentication) {
        if (checkAccess(id, authentication)) {
            setListAndUser(model, blogService.getSavedBlogId(id), userService.get(id));
            return "author";
        }
        return "redirect:/author";
    }

    @RequestMapping(value = "/show-blog")
    public String showBlog(@RequestParam Integer id, Model model) {
        Blog blog = blogService.get(id);
        blog.setAuthorName(userService.get(blog.getAuthorId()).getUserName());
        List<Comment> comments = blog.getCommentList();
        comments.forEach(comment -> comment.setUserId(userService.getId(comment.getUserName())));
        model.addAttribute("blog", blog);
        model.addAttribute("commentOb", new Comment());
        model.addAttribute("commentList", comments);
        return "show-blog";
    }

    @PostMapping(value = "/blog/publish/{id}")
    public String publishBlog(@ModelAttribute Blog blog, @ModelAttribute Tag tag, @PathVariable Integer id,
                              Authentication authentication) {
        Set<Tag> tagSet;
        blog.setUpdatedAt(new Date());
        if (id == 0) {
            if (!blog.getSaveDraft()) {
                blog.setPublishedAt(new Date());
                logger.info("publish blog {} :", id);
            }
            blog.setAuthorId(userService.getId(authentication.getName()));
            tagSet = tagService.save(tag.getName(), blog);
        } else {
            if (blog.getSaveDraft() != blogService.get(id).getSaveDraft() && !blog.getSaveDraft()) {
                blog.setPublishedAt(new Date());
            }
            Blog blog1 = blogService.get(id);
            tagSet = tagService.save(tag.getName(), blog1);
            blog = blogService.update(id, blog);
            logger.info("update blog {} :", id);
        }
        blog.setTagSet(tagSet);
        blogService.save(blog);
        return "redirect:/";
    }

}
