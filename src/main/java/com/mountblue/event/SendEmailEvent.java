package com.mountblue.event;

import com.mountblue.entity.User;
import org.springframework.context.ApplicationEvent;

public class SendEmailEvent extends ApplicationEvent {

    private User user;
    private String message;
    private String subject;

    public SendEmailEvent(Object source, User user, String message, String subject) {
        super(source);
        this.user = user;
        this.message = message;
        this.subject = subject;
    }

    public User getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public String getSubject() {
        return subject;
    }
}
